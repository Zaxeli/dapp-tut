Address:                               0x47b18b482e714306adcda81bd6f5ef14139f9a82  (init with 50 ZEM)
Address(public deadline,beneficiary):  0xc47e5c3f55a65cda156ad4e23dcc085529a617cd  (init with 100 ZEM)
Address (after testing): 	       0xbc6851da782c9135e8873e496d7565ad6556f182  (init with 100 ZEM)

pragma solidity ^0.4.16;

interface ZEM {
    function transfer(address receiver, uint amount);
}

contract Crowdsale {
    
    address owner;
    
    address public beneficiary;
    uint fundingGoal;
    uint public crowdSaleDeadline;
    uint totalAmountRaised;
    uint tokenPrice;
    ZEM public token;
    mapping(address => uint) balanceOf;
    bool fundingGoalReached = false;
    //bool saleClosed = false;
    
    
    /**
     * Constructor
     * ifSuccessfulSendTo: Address where funds should be sent if sale reaches target
     * goalInEther: What is the target goal for the crowdsale in ethers.
     * durationInMinutes: How long will the crowdsale be running.
     * tokenPriceInEther: How much does each token cost
     * addressOfToken: Where is the token contract deployed.
     */
    constructor(
        address _ifSuccessfulSendTo,
        uint _goalInEther,
        uint _durationInMinutes,
        uint _tokenPriceInEther,
        address _addressOfToken
    ) public {
        beneficiary = _ifSuccessfulSendTo;
        fundingGoal = _goalInEther * 1 ether;
        crowdSaleDeadline = now + _durationInMinutes * 1 minutes;
        tokenPrice = _tokenPriceInEther * 1 ether;
        token = ZEM(_addressOfToken);
        
        owner = msg.sender;
    }    

    /**
     * Fallback function
     *
     * Default function which gets called when someone sends money to the contract. Will be used for joining sale.
     */
    function () public payable beforeDeadline {
        totalAmountRaised += msg.value;
        balanceOf[msg.sender] += msg.value;
        token.transfer(msg.sender, msg.value / tokenPrice);
    }
    
    /**
     * Modifier used to check if crowdsale is open
     */
    modifier beforeDeadline() {
        require(now<crowdSaleDeadline);
        _;
    }
    
    /**
     * Modifier used to check if deadline for crowdsale has passed
     */
    modifier afterDeadline() {
        require(now>=crowdSaleDeadline);
         _;
    }

    /**
     * Check if the funding goal was reached. Will only be checked if afterDeadline modifier above is true.
     *
     */
    function checkGoalReached() afterDeadline {
        if(totalAmountRaised>=fundingGoal){
            fundingGoalReached = true;
        }
    }


    /**
     * Withdraw the funds
     *
     * Will withdraw the money after the deadline has been reached.
     * If the goal was reached, only the owner can withdraw money to the beneficiary account.
     * If the goal was not reached, everyone who participated can withdraw their share.
     */
    function safeWithdrawal() afterDeadline {
        checkGoalReached();
        if(fundingGoalReached && msg.sender==beneficiary){
            if(!beneficiary.send(totalAmountRaised)){
                fundingGoalReached=false;
            }
        }
        else if(!fundingGoalReached){
            uint amount = balanceOf[msg.sender];
            balanceOf[msg.sender] = 0;
            if(!msg.sender.send(amount)){
                balanceOf[msg.sender] = amount;
            }
        }
    }
}