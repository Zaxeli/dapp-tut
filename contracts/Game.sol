pragma solidity ^0.4.20;

contract Game{
    
    uint time;
    
    function makeGuess() public payable returns(bool){
        time = block.timestamp;
        
        if(time%2==0){
            msg.sender.transfer(msg.value*2);
        }
        return time%2==0;
    }
    
    function getBalance() public view returns(uint){
        return address(this).balance;
    }
    
    function () public payable{} //fallback
}